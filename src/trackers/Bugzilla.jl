"Allows retrieving bug information from Bugzilla"
module Bugzilla

using HTTP
using HTTP.URIs: escapeuri, URI
using Markdown
import Markdown: MD, Link, Paragraph, LineBreak, Bold, Italic
using JSON
import IJulia

import JDP.IOHelpers: prompt
import JDP.Conf
using JDP.Tracker
import JDP.Functional: cmap
using JDP.Repository
using JDP.BugRefs
using JDP.Metarules

mutable struct Session <: Tracker.AbstractSession
    host::String
    scheme::String
    user::String
    password::String
    token::Union{String, Nothing}

    function Session(host::String, user::String, pass::String)
        new(host, "https", user, pass, nothing)
    end
end

function json_rpc(uri, method, params)
    nonce = time_ns()
    js = JSON.json((id = nonce,
                    method = method,
                    params = [params]))
    ret = HTTP.post(uri, ["content-type" => "application/json"], js).body |>
        String |> JSON.parse

    isnothing(ret["error"]) || error("Bugzilla RPC Error: $(ret["error"]["message"])")
    ret["id"] == nonce || error("Bugzilla nonce did not match: $(ret["id"]) ≠ $nonce")

    ret["result"]
end

json_rpc(ses::Session, method, params) =
    json_rpc(URI(scheme=ses.scheme, host=ses.host, path="/jsonrpc.cgi"),
             method,
             isnothing(ses.token) ? params : (Bugzilla_token = ses.token, params...))

function login(host_tla::String)::Union{Session, Nothing}
    conf = Conf.get_conf(:trackers)["instances"][host_tla]

    if conf["api"] != "Bugzilla"
        throw("$host_tla is not a Bugzilla instance, but instead $(conf["api"])")
    end

    user = get(conf, "user") do
        prompt("User Name")
    end

    pass = get(conf, "pass") do
        prompt("Password"; password=true)
    end

    login(conf["host"], user, pass)
end

login(host::String, user::String, pass::String)::Union{Session, Nothing} =
    login!(Session(host, user, pass))

function login!(ses::Session)
    ses.token = json_rpc(ses, "User.login", (
        login = ses.user,
        password = ses.password,
        restrict_login = true
    ))["token"]

    ses
end

Tracker.ensure_login!(t::Tracker.Instance{Session}) = if t.session == nothing
    t.session = login(t.tla)
else
    login!(t.session)
end

get_raw_bug(ses::Session, id::Int64)::Dict =
    json_rpc(ses, "Bug.get", (ids = [id],))["bugs"][1]

get_raw_bug(from::String, id::Int64)::Dict =
    get_raw_bug(Tracker.login(from), id)

get_raw_comments(ses::Session, id::Int64)::Vector =
    json_rpc(ses, "Bug.comments", (ids = [id],))["bugs"] |>
             values |> first |> values |> first

abstract type Item <: Repository.AbstractItem end

struct Comment <: Item
    id::Int64
    text::String
end

Comment(c) = Comment(c["id"], c["text"])
Comments(c) = [Comment(c)]
Comments(cs::Vector) = [Comment(c) for c in cs]

mutable struct Bug <: Item
    id::Int64
    severity::String
    priority::String
    status::String
    short_desc::String
    arches::Vector{String}
    comments::Vector{Comment}
end

Bug(x::Dict, cs::Array) = Bug(x["id"],
                              x["severity"],
                              x["priority"],
                              x["status"],
                              x["summary"],
                              [x["platform"]],
                              Comments(cs))

Base.show(io::IO, ::MIME"text/markdown", bug::Bug) =
    write(io, "**", bug.priority, "** _", bug.severity, "_ ", bug.status, ": ",
          bug.short_desc)

Metarules.extract(bug::Bug) = Metarules.extract(bug.short_desc)

function Repository.refresh(t::Tracker.Instance{Session}, bref::BugRefs.Ref)::Bug
    ses = Tracker.ensure_login!(t)
    id = parse(Int64, bref.id)
    bug = Bug(get_raw_bug(ses, id), get_raw_comments(ses, id))

    @info "$(t.tla): GET bug $(bref.id)"
    Repository.store("$(t.tla)-bug-$(bref.id)", bug)

    bug
end

function Repository.fetch(::Type{Bug}, bref::BugRefs.Ref)::Union{Bug, Nothing}
    if !(bref.tracker isa Tracker.Instance{Session})
        @error "$bref does not appear to be a Bugzilla Bug"
        return nothing
    end

    bug = Repository.load("$(bref.tracker.tla)-bug-$(bref.id)", Bug)

    if bug != nothing
        bug
    else
        Repository.refresh(bref.tracker, bref)
    end
end

Repository.fetch(::Type{Bug}, ::Type{Vector}, from::String)::Vector{Bug} =
    Repository.mload("$from-bug-*", Bug)

end # module
