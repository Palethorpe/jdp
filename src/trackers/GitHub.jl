module GitHub

import JSON
import HTTP

import JDP.Conf
import JDP.Tracker
import JDP.Repository
import JDP.BugRefs
import JDP.Metarules

mutable struct Session <: Tracker.AbstractSession
    uri::String
    headers::Vector{Pair{String, String}}
end

Tracker.ensure_login!(t::Tracker.Instance{Session}) = if t.session == nothing
    conf = Conf.get_conf(:trackers)["instances"][t.tla]
    url = get(conf, "url", "$(t.scheme)://api.$(t.host)")
    headers = ["Accept" => "application/json",
               "Content-Type" => "application/json",
               "User-Agent" => get(conf, "login", ""),
               "Authorization" => "Bearer " * get(conf, "token", "0000")]

    t.session = Session("$url/graphql", headers)
else
    t.session
end

function post_query(ses::Session, query::String)
    HTTP.post(ses.uri, ses.headers, JSON.json(:query => query);
              status_exception=true).body |> String |> JSON.parse
end

query_error(q) = error("Error querying GitHub:\n", q["errors"])
function get_raw(ses::Session, owner, repo, number::Int)
    q = """{
    repository(owner: "$owner", name: "$repo") {
        issue(number: $number) {
            id
            title
            body
            state
            comments(last: 100) {
                nodes {
                    id
                    body
                }
            }
        }
        pullRequest(number: $number) {
            id
            title
            body
            state
            comments(last: 100) {
                nodes {
                    id
                    body
                }
            }
        }
    }
}
"""

    q = post_query(ses, q)
    d = get(q, "data") do
        query_error(q)
    end
    r = get(d, "repository") do
        query_error(q)
    end
    i = get(r, "issue", nothing)
    i = i === nothing ? get(r, "pullRequest", nothing) : i
    i === nothing && query_error(q)
    i
end

function get_raw(ses::Session, owner, repo, oid::AbstractString)
    q = """{
    repository(owner: "$owner", name: "$repo") {
        object(oid: "$oid") {
            id
            __typename
            ... on Commit {
                title: messageHeadline
                body: messageBody
                comments(last: 100) {
                    nodes {
                        id
                        body
                    }
                }
            }
        }
    }
}
"""

    q = post_query(ses, q)
    d = get(q, "data") do
        query_error(q)
    end
    r = get(d, "repository") do
        query_error(q)
    end
    i = get(r, "object", nothing)
    isnothing(i) && query_error(q)
    t = i["__typename"]
    t != "Commit" && error("GitHub: $t object is not a Commit: $owner/$repo#$oid")
    i["state"] = "";
    i
end


function parse_id(id::String)
    m = match(r"^([\w-]+)/([\w-]+)#(\d+)$", id)

    if isnothing(m)
        m = match(r"^([\w-]+)/([\w-]+)/issues/(\d+)$", id)
    end

    if isnothing(m)
        m = match(r"^([\w-]+)/([\w-]+)#([a-z0-9]+)$", id)

        m[1], m[2], m[3]
    else
        m[1], m[2], parse(Int, m[3])
    end
end

abstract type Item <: Repository.AbstractItem end

struct Comment <: Item
    id::String
    text::String
end

struct Bug <: Item
    id::String
    status::String
    short_desc::String
    comments::Vector{Comment}
end

Bug(i::Dict) = Bug(i["id"], i["state"], i["title"],
                   [Comment("", i["body"]),
                    (Comment(c["id"], c["body"]) for c in i["comments"]["nodes"])...])

Metarules.extract(bug::Bug) = Metarules.extract(bug.short_desc)

Base.show(io::IO, ::MIME"text/markdown", issue::Bug) =
    write(io, issue.status, ": ", issue.short_desc)

function Repository.refresh(t::Tracker.Instance{Session}, bref::BugRefs.Ref)::Bug
    ses = Tracker.ensure_login!(t)
    issue = get_raw(ses, parse_id(bref.id)...) |> Bug

    @info "$(t.tla): GET bug $(bref.id)"
    Repository.store("$(t.tla)-issue-$(bref.id)", issue)

    issue
end

function Repository.fetch(::Type{Bug}, bref::BugRefs.Ref)::Bug
    bref.tracker isa Tracker.Instance{Session} ||
        error("$bref does not appear to be a GitHub issue")

    issue = Repository.load("$(bref.tracker.tla)-issue-$(bref.id)", Bug)

    issue === nothing ? Repository.refresh(bref.tracker, bref) : issue
end


end #module
