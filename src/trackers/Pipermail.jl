module Pipermail

import HTTP

import JDP.Conf
import JDP.Tracker
import JDP.Repository
import JDP.BugRefs
import JDP.Metarules

mutable struct Session <: Tracker.AbstractSession
    uri::String
end

Tracker.ensure_login!(t::Tracker.Instance{Session}) = if t.session === nothing
    conf = Conf.get_conf(:trackers)["instances"][t.tla]
    t.session = Session("$(t.scheme)://$(t.host)/pipermail/$(conf["name"])")
else
    t.session
end

function get_raw_mail(ses::Session, id::String)
    HTTP.get("$(ses.uri)/$id.html"; status_exception=true).body |> String
end

struct Bug <: Repository.AbstractItem
    short_desc::String

    Bug(html::String) = new(match(r"<title>(.*?)</title>"si, html)[1] |> strip)
end

Base.getproperty(mail::Bug, p::Symbol) = if p === :status
    ""
else
    getfield(mail, p)
end

Metarules.extract(::Bug) = Metarules.Rule[]

Base.show(io::IO, ::MIME"text/markdown", mail::Bug) = write(io, mail.short_desc)

function Repository.refresh(t::Tracker.Instance{Session}, bref::BugRefs.Ref)::Bug
    ses = Tracker.ensure_login!(t)
    mail = get_raw_mail(ses, bref.id) |> Bug

    @info "$(t.tla): GET mail $(bref.id)"
    Repository.store("$(t.tla)-mail-$(bref.id)", mail)

    mail
end

function Repository.fetch(::Type{Bug}, bref::BugRefs.Ref)::Bug
    bref.tracker isa Tracker.Instance{Session} ||
        error("$bref does not appear to be a Pipermail archive item")

    mail = Repository.load("$(bref.tracker.tla)-mail-$(bref.id)", Bug)

    mail === nothing ? Repository.refresh(bref.tracker, bref) : mail
end

end
